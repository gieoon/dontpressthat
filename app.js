//SERVERSIDE CODE
//library imports
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();
var serv = require('http').Server(app);
//FOR LOCAL TESTING
// var server = app.listen(8082, function() {
//  var port = server.address().port;
//  console.log('Server running at port %s', port);
//  console.log("Hello World");
// })

//OPENSHIFT CONFIGURATIONS
// var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
// var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
// server.listen(server_port, server_ip_address, function(){
//   console.log("Listening on " + server_ip_address + ", port " + server_port)
// });

//HEROKU CONFIGURATIONS
var port = process.env.PORT || 8080;
console.log("PORT: ", port);


//const io = require('socket.io')();
const socketIO = require('socket.io');
var index = require('./routes/index');
//const index = path.join(__dirname, 'index.html');
var users = require('./routes/users');

var firebase = require('firebase');

//local constants
const UPDATE_RATE = 1000;
const MAX_USERNAME_LENGTH = 25;

//const server = express()
serv
//  .use((req, res) => res.sendFile(index) )
  .listen(port, () => console.log('Listening on ${ PORT }' ));

//const io = socketIO(server);
const io = socketIO(serv);

//local variables
var b_button = false;
var mButtonOwner = 0;
var mPlayers = [];
var n_lastPressTime = 0;
var TIME_DIFFERENCE = 3000;


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// sets port 8080 to default or unless otherwise specified in the environment
//app.set('port', process.env.PORT || 8080);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));//using morgan to log data through
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//for routing??
app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// app.get('/', function(req, res){
//     res.send('hello world');
// });



function initFirebase(){
  var config = {
    apiKey: "AIzaSyD6XJ6SUg_Zk2lRukm9jypmGtSjDzHDQ58",
    authDomain: "dont-press-that.firebaseapp.com",
    databaseURL: "https://dont-press-that.firebaseio.com",
    projectId: "dont-press-that",
    storageBucket: "",
    messagingSenderId: "1068081005377"
  };
  firebase.initializeApp(config);
}

initFirebase();
broadCastLeaderBoardState();

io.on('connection', (client) => {
  //emit events to the client.
  client.send(client.id);
  console.log("Sending for client to initialize");
  client.emit('initialize', client.id);
  console.log("CLient.id: ", client.id);

  client.on('disconnect', function(){
    console.log("original length was; ", mPlayers.length);
    for(i in mPlayers){
      if(mPlayers[i].id == client.id){
        //remove from database
        firebase.database().ref(mPlayers[i].push_key).remove();

        mPlayers[i] = null;
        mPlayers.splice(i, 1);
        console.log("Shortened players array, new length is: ", mPlayers.length);
        io.emit('users_count', mPlayers.length);
       
        break;
        //broadcast will send to everyone BUT this socket.
      }
    }
  });

  client.on('buttonPressed', (interval) => {
    //console.log('client has pressed the button: ', checkPointsFromPress(client.id));
    client.emit('points', checkPointsFromPress(client.id));
    //write to database here
    var t_player = findPlayerFromClient(client);
    //check fi push key has been set
    if(t_player !== undefined){
      if(t_player.push_key == 0){
        var push_key = firebase.database().ref().push().key;
        t_player.push_key = push_key;
      }
      //update score stored in database
      firebase.database().ref(t_player.push_key).set({
          username: t_player.name,
          //* -1 to reverse the sorting order...
          score: t_player.score * -1
      });
    }
    updateLeaderBoardScores();//triggered by lsitener, not by a button press

    enablePlayerButtonPress(client);
  });
  client.on('initialize', (name) =>{
    console.log("received name: ", name);
    var t_player = new Player(client.id, name);
    var push_key = firebase.database().ref().push().key;
    t_player.push_key = push_key;
    mPlayers.push(t_player);
    io.emit('users_count', mPlayers.length);
    updateLeaderBoardScores();
  });
  client.on('username', (name) =>{
    console.log("RECEIVED USERNAME UPDATE: ", name);
    var new_name = name.trunc();
    updatePlayerName(client.id, new_name);
    updateLeaderBoardScores();
  });
});

//const port = 8000;
//FOR BEFORE USING WEBSOCKETS...NOW THIS DOES NOT WORK 
//io.listen(port);
//console.log('listening on port ', port);

function broadCastLeaderBoardState(){
  setInterval(function(){
   // updateLeaderBoardScores();
  }, UPDATE_RATE);
}

function checkPointsFromPress(clientId){
  var t_point = 0;
  var n = checkGlobalButtonPress();
  if(n != 0){
    //find the clientId
    for(i = 0; i < mPlayers.length; i++){
      console.log("mPlayers length is: ", mPlayers.length);
      if(mPlayers[i].id == clientId){
        mPlayers[i].score++;
        console.log("incrementing score");//TODO this is happening twice isntead of once!!
        t_point++;
        return new playerPackage(mPlayers[i].score, n);
      }
    }
  }
  //0 means a loss;
  if(t_point == 0 && mPlayers.length > 0){
    mPlayers[i].score = 0;
  }
  return 0;
}

String.prototype.trunc = function(){
  console.log("this: ", this);
  console.log("this.length: ", this.length);
  if (this.length <= MAX_USERNAME_LENGTH - 10) {
    console.log("less than length is true");
    return this;
  }
  //truncate at last boundary of the string
  var subStr = this.substring(0, MAX_USERNAME_LENGTH - 10);
  console.log("subStr is: ", subStr);
  return (
    subStr + '...' //horizontal ellipses.
  );
}

function truncateUsername(name){
  //if(name.length <= MAX)
}

function calculatePlayerRank(client){
  var count = 0;
  var rank = 0;
  var t_player = findPlayerFromClient(client);
  var rankQuery = firebase.database().ref().orderByChild('score');
  rankQuery.once("value", function(all_snapshot){
    all_snapshot.forEach(function(push_snapshot){
      count++;
      if(push_snapshot.key == t_player.push_key){
        rank = count;
      }
    });
  }).then(function(success){
    if(success){
      console.log("EMITTING RANK: ", rank);
      client.emit('playerrank', rank);
    }
    else{
      client.emit('playerrank', "Non Existent");
    }
  });

}

var topUsersQuery = firebase.database().ref();//.orderByChild('score').limitToFirst(10);
topUsersQuery.once("value", function(snapshot){
  updateLeaderBoardScores();
});
function updateLeaderBoardScores(){
   var count = 1;
    var scores = [];
    //update the leaderboards
    var topUsersQuery = firebase.database().ref().orderByChild('score').limitToFirst(10);
    //this shoudl be called every time the value changes, consistently.
    topUsersQuery.once("value", function(snapshot){
      snapshot.forEach(function(score_snapshot){
        //console.log("SCORE SNAPSHOT KEY: ", score_snapshot.key);
        //console.log('COUNT: ', count);
        //console.log("USERNAME: ", score_snapshot.child("username").val());
        var t_score = new Score(
          count,
          score_snapshot.child("username").val(),
          score_snapshot.child("score").val() * -1 // == 0 ? 0 : * score_snapshot.child("score").val() * -1;
        );
        scores.push(t_score);
        //console.log(scores);
        count++;
      });
    }).then(function(success){
        if(success){
          //send eladerboard to all users
          io.emit('leaderboard_update', scores);
          console.log("EMITTED FOR LEADERBOARDUPDATE: ", scores);
        }
        else if(!success){
          console.log("ERROR TRYING TO RETRIEVE LEADERBOARD DETAILS FROM DATABASE: ");
        }
    });
    //FIND SCORE OF OWN PLAYER AND EMIT NOW.
}

function checkGlobalButtonPress(){
  var t_date = Date.now();
  console.log("date.now(): ", Date.now());
  console.log("Time diff is: ", Date.now() - n_lastPressTime);
  if(t_date - n_lastPressTime > TIME_DIFFERENCE){
    n_lastPressTime = t_date;
    return n_lastPressTime;
  }
  n_lastPressTime = t_date;
  return 0;
}

function playerPackage(points, time){
  this.points = points;
  this.time = time;
}

function enablePlayerButtonPress(client){
  setTimeout(function(){
      client.emit('enableButtonPress', null);
  }, TIME_DIFFERENCE);
}

function updatePlayerName(clientId, name){
  for(i in mPlayers){
    if(mPlayers[i].id == clientId){
      mPlayers[i].name = name;
      var ref = firebase.database().ref(mPlayers[i].push_key);
      ref.once("value", function(user_snapshot){
          user_snapshot.child("username").ref.set(name);
          user_snapshot.child("score").ref.set(-0);
      });
      break;
    }
  }
}

function findPlayerFromClient(client){
  var t_player;
  for(i in mPlayers){
    if(mPlayers[i].id == client.id){
      t_player = mPlayers[i];
      break;
    }
  }
  return t_player;
}


function ButtonOwner(mPlayer){
  this.owner = mPLayer;
}

function Player(id, name){
  this.id = id;
  this.name = name;
  //every new player's socre starts at 0.
  this.score = 0;
  this.push_key = 0;
}

//for packet emitting score to clients
function Score(rank, username, score){
  this.rank = rank;
  this.username = username;
  this.score = score;
}





module.exports = app;
