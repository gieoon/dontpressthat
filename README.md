## DontPressThatButton Server
Implementation of a NodeJS server which communicates with a frontend and data processing.

This is a game where each player presses a button, qand if they have pressed it when no one else has pressed it in the last three seconds, they get incremental points.
However, if someone else has pressed it, their points will be reset to 0

The live version was hosted on Heroku